#!/bin/bash
if ! which redis-server > /dev/null; then
   echo -e "Command not found! Install? (y/n) \c"
   #read
   if [[ 100 -gt 10 ]]; then
      sudo dpkg -i redis-tools_5%3a5.0.14-1+deb10u2_arm64.deb libhiredis0.14_0.14.0-3_arm64.deb libjemalloc2_5.1.0-3_arm64.deb redis-tools_5%3a5.0.14-1+deb10u2_arm64.deb liblua5.1-0_5.1.5-8.1+b2_arm64.deb redis-server.deb lua-bitop_1.0.2-5_arm64.deb lua-cjson_2.1.0+dfsg-2.1_arm64.deb
   fi
fi

s1="websocket-streaming"
path="/home/pi/Documents/anpr"
file="websocket-streaming/websocket_live_streaming_service.py" 
s1_status="not_exists"
sudo systemctl is-active --quiet $s1 && s1_status="exists"
echo $s1_status

if [ $s1_status = "not_exists" ];
then
    echo "[Unit]" >> /etc/systemd/system/$s1.service
    echo "Description=streaming_service" >> /etc/systemd/system/$s1.service
    echo "[Service]" >> /etc/systemd/system/$s1.service
    echo "User=pi" >> /etc/systemd/system/$s1.service
    echo "Group=pi" >> /etc/systemd/system/$s1.service
    echo "WorkingDirectory=$path" >> /etc/systemd/system/$s1.service
    echo "ExecStart=/usr/bin/python3 $path/$file" >> /etc/systemd/system/$s1.service
    echo "Restart=always" >> /etc/systemd/system/$s1.service
    echo "StandardOutput=inherit" >> /etc/systemd/system/$s1.service
    echo "[Install]" >> /etc/systemd/system/$s1.service
    echo "WantedBy=multi-user.target" >> /etc/systemd/system/$s1.service
    echo "$s1 serivce made successfully"
    sudo systemctl daemon-reload
    sudo systemctl start $s1
    sudo systemctl enable $s1
else
    echo "Service $s1 already exists"
fi

tabs 4
path=/etc/apache2/apache2.conf

s="ServerSignature" 

if grep -q $s $path;then
    echo "Contains pattern"
else
    echo "Does not contain pattern"
    echo "<Directory /var/www/>" >> $path
    echo -e "\tAllowOverride All" >> $path
    echo -e "\tOptions -Indexes" >> $path
    echo -e "\tServerSignature off" >> $path
    echo "</Directory>" >> $path 
    sudo systemctl restart apache2.service
fi


protobuf_source_file="/home/pi/.local/lib/python3.7/site-packages/protobuf-3.20.3-py3.7-nspkg.pth"
original_file="protobuf-3.20.3-py3.7-nspkg.pth"

if test -f "$protobuf_source_file"; then 
    echo "Protof already exist"
else
    cp "$original_file" "$protobuf_source_file"
fi


google_source_file="/home/pi/.local/lib/python3.7/site-packages/google/protobuf/internal/builder.py"
google_source_folder="/home/pi/.local/lib/python3.7/site-packages/"
google_original_folder="google"

if test -f "$google_source_file"; then 
    echo "google already exist"
else
    cp -r  "$google_original_folder" "$google_source_folder"
fi